## PaintVR

There are several programs in the PaintVR directory, each developed for a different usecase. This is not the development-repo (source code) but the repo with executable programs.
They are executable in Windows 10 (64 bits). The pre-requisites are mentioned below.

The following programs are available:

1. Minimal configuration Paint VR
2. Headless configuration Paint VR

### Minimal configuration Paint VR
The program lets you record paint-strokes with a controller, the strokes are stored in a database. Two car-panels and a car-wheel are added as a target. 
In this configuration you do not get feedback of painted areas on the targets. There is a visual representation of the paint plume in the air, and you can choose its color (red, green, blue) or turn it off.
The location of the targets is fixed. The initial position of the player (teleporting is not enabled) can be given, by default it will start in the center of all the targets.

The minimal configuration Paint VR is tested with a Vive Pro and a Vive controller.

### Headless configuration Paint VR
The program lets you record paint-strokes with a real paint-gun which has a Vive tracker placed on it, which interfaces with it mechanically and electrically (pogo-pins used for input of the paint-gun lever).
The paint-gun is capable of making real paint strokes and those are stored in a database. Two additional trackers can be mounted on a paintable product, or elsewhere, and their position will be stored in the database too.
On a monitor behind a computer the tracker-configuration must be defined. The functionality of the VR headset is limited to monitoring.

A more elaborate description of the programs is given in the relevant directory.

---

## Preconditions

1. Access to a MongoDB database (locally or in the cloud).
2. Vive Pro headset and a Vive controller 
3. Additional hardware is required for the headless configuration
3. SteamVR (beta version required for the headless configuration)
3. Windows 10 - 64 bits 
4. Computer with a graphical card suitable for VR (no specs available - tested with a Quatro RTX 4000)
