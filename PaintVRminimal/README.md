## PaintVR Minimal Configuration

The minimal configuration Paint VR is tested with a Vive Pro and a Vive controller.
The program lets you record paint-strokes with a controller, the strokes are stored in a database. Two car-panels and a car-wheel are added as a target. 
In this configuration you do not get feedback of painted areas on the targets. There is a visual representation of the paint plume in the air, and you can choose its color (red, green, blue) or turn it off.
The location of the targets is fixed. The initial position of the player (teleporting is not enabled) can be given, by default it will start in the center of all the targets.

In the database the position and orientation of all the tracked positions are given wrt the global VR-world. Also, a world-reference location is present. Also the tracked locations will be stored in the database wrt this location.
This will be usefull if you want to deploy a robot. You can position the world-ref-location in the intended robot-location, and thus
all the tracked positions will be relative to this location. For the world-ref-location also the orientation must be given, thus must be done as a quaternion.

Bear in mind that Unity uses a left-hand-coordinate system where +Y points vertical upward.

There are two options regarding tracking. You can track the positions with a fixed timestep, or with a fixed distance. For both options the time of the tracked positions is stored in the database.
Be aware that when using a fixed timestep the time between the steps may vary, so use the stored time-data for analysis and do not assume a fixed timestep.
The refresh-rate is about 90 Hz so specifying a timestep close to or smaller than this will not give you more data. 
The tested timestamp was 0.1 second (10Hz) and that worked okay, there was no need to have more accurate data.

When a fixed distance is used good results have been achieved with distance to 1 cm. It has not been tested with a smaller fixed distance but it may be possible.
All settings must be set in a configuration file.


---

## Preconditions

1. Access to a MongoDB database (locally or in the cloud).
2. Vive Pro headset and one Vive controller (right hand).
3. SteamVR (beta not required, this is only required for the headless configuration)
3. Windows 10 - 64 bits 
4. A graphical card suitable for VR (no specs available - tested with a Quatro RTX 4000)

---

## Install and running
Install everything from this git-repository on the computer with a working VR-configuration.
Then goto directory PaintVRminimal and run PaintVR.exe. 


## The configuration file
The configuration file is a json-file name config.json. It must be placed in the same directory as PaintVR.exe.
It is read at the start of the application, so alterations when the program is running is not allowed.

This is the default configuration. 



```json
{ "Database" :	{ "ClientConnection" : "mongodb://127.0.0.1:27017",
                  "DatabaseName" : "test",
                  "DBCollection" : "Prorob3"
                },
    "PlayerStartLocation" : { "x" : -2.3,
                              "y" : 0,
                              "z" : -13.3 
                            },
    "TrackingUnit" : "distance",
    "TrackingQuantity" : 0.1,   
    "WorldRefLocation" :  { "x" : -2.3,
                            "y" : 0,
                            "z" : -13.3,
                            "qx" : 0,
                            "qy" : 0,
                            "qz" : 0,
                            "qw" : 1
                           }, 
    "PaintPlume" : "green"
}
```

### Database
The MongoDB database must already be possible. If you have it installed locally than most likely (default) 
it is installed in 127.0.0.1:27017.
The database must already be present, here it is called "test".
The DBCollection must not be necessarily available. If it already exists than new paint-strokes will be added to it.
If the DBCollection does not exist than it will be created by the program if allowed.

```json
{"Database" :	{ "ClientConnection" : "mongodb://127.0.0.1:27017",
                  "DatabaseName" : "test",
                  "DBCollection" : "Prorob3"
                }}
```

### PlayerStartLocation
The startlocation of the player can be changed. Default position is between the targets and should be okay. Orientation can not be given.
The location parameters must be numbers as indicated below. It is meter.
```json
  {  "PlayerStartLocation" : { "x" : -2.3,
                              "y" : 0,
                              "z" : -13.3 
                            }}
```

### Tracking
There is the ability to track with a fixed distance. Than trackingunit must be "distance". Than trackingquantity is the fixed distance in meter.
There is the ability to track with a fixed time. Than trackingunit must be "time". Than trackingquantity is the fixed time in seconds.


```json
  {  "TrackingUnit" : "distance",
    "TrackingQuantity" : 0.1,   }
```

### WorldRefLocation
The worldreflocation is indicated in the playfield with a square box with sides of 10 cm. Its location and orientation (quaternion) must be given (with numbers as indicated below). 
Locations are in meter (x,y,z). Quaternion is (qx,qy,qz,qw).


```json
{    "WorldRefLocation" :  { "x" : -2.3,
                            "y" : 0,
                            "z" : -13.3,
                            "qx" : 0,
                            "qy" : 0,
                            "qz" : 0,
                            "qw" : 1
                           }}
```
### PaintPlume
There are four options for paint plume:

1. "red"
2. "green"
3. "blue"
4. "off"

