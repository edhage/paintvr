## PaintVR Headless Configuration

The Paint VR headless configuration is tested with a paint-gun mounted with a tracker, and the use of a second tracker (used as reference), plus the
Vive Pro headset. A Vive controller is used to set the controller bindings in SteamVR.

![Paintgun held in hand](Pictures/paintgun_held_in_left_hand.png){height=240 width=302}

The program is developed so that a external hardware (the paintgun) can be tracked and the information stored in a database. This is a situation with physical obstacles in the playing field (parts to be painted, paint equipment,etc.) so the use of a headset should not be required.
This is done by making a traditional application that is operated from a computer using a monitor and a keyboard. In this application you assign the devices to the tracker instances. Minimally you would assign the correct device to the paint gun tracker (which is the tracker mounted on the paint gun).
There are two uses for the headset.

1. Monitoring the paint process
2. Assigning the correct tracker binding in SteamVR 

> The headset is required because SteamVR cannot function without it.


The application looks like the picture below. 
![Application to set up the hardware configuration](Pictures/paintheadless_VRscherm.png)


## Preconditions

1. Access to a MongoDB database (locally or in the cloud).
2. Vive Pro headset and minimal one Vive tracker with mechanical interfacing on the paint-gun
3. Additionally 2 extra Vive trackers are supported. 
4. A Vive controller may be required for tracker binding in SteamVR.
5. SteamVR beta must be installed
6. Windows 10 - 64 bits 
7. A graphical card suitable for VR (no specs available - tested with a Quatro RTX 4000)

Here you will find the [SteamVR beta installation instruction](https://www.vive.com/mea-en/support/vive/category_howto/optin-to-steamvr-beta.html) 

---


##  Definition Poses and Database

In the database the poses (position and orientation) of the tracked paint-gun and upto two additional trackers are given wrt the global VR-world. 
The VR-world has its origin determined by the playarea [as defined in the room-setup](https://www.vive.com/ca/support/vive-pro-hmd/category_howto/setting-up-room-scale-play-area.html) and it is indicated visually with the big coordinate system within a large target board.
The origin is typically within the center of the basestations on floor-level.

In the database every paint stroke will be stored as a document. Each document has the following layout:

1. strokename - Generated name of the stroke and it is the same as shown in the application when the stroke is executed.
2. PosesRos - Poses of the paint-gun in the [ROS](https://www.ros.org/) format.
3. PosesGlobal - Poses of the paint-gun in the global VR-world 
4. PosesTracker1 - Poses of the Reference tracker in the global VR-world 
5. PosesTracker2 - Poses of the Additional tracker in the global VR-world 
6. PosesRelRobot - Poses of the paint-gun relative to the world-ref-location as defined in the configuration file
7. PosesRelTracker1 - Poses of the paint-gun relative to the Reference tracker
8. Time - Time in seconds of the poses since the start of the application
9. LinePositions - Positions of the paint-gun in the global VR-world to define the visual stroke (does not match with Time)

Bear in mind that Unity uses a left-hand-coordinate system where +Y points vertical upward. This is the definition of the global VR-world.
Typical entries in the MongoDB database are shown below.

![MongoDB](Pictures/MongoDB_entries.png)


---

## Installing
Install everything from this git-repository on the computer with a working VR-configuration.
Then goto directory PaintVRheadless and run PaintHeadless.exe. 

Configuration is done in two parts. Configuration of database settings and a static world-reference is done in a configuration file.
The configuration of the trackers is done in the application itself.

## Configuring trackers in the application
When the application is ran you will get two displays (if you have two monitors connected). 

### The first display
The first display is the display in which configure the devices. 
![First display](Pictures/headless_main_monitor.png)

The devices are shown in the SteamVR application which is running in parallel (it will be started once you start the application, or could already be running).
The devices are numbered, but get assigned a number in which they are switched on (but not always). This consists of the base stations, the controllers and the trackers.
In the application you need to find the correct device to match the paint gun, or up to two other trackers.
This is done by trial and error. So assign a device in the dropdown menu for "Paint Gun Tracker" and physically move the paint gun. If a model of the paint gun moves in VR than you assigned the correct one.
In the image above you can see that Device 4 has been assigned to the Paint Gun Tracker.

The paint gun may not be visible on the first screen. This is the case you can place the paint gun in the center of the play area. It should be visible there.
If there is a physical obstacle you can also pan the camera by using the keyboard keys 'F' to pan to the left, or 'G' to pan to the right.

Once the correct device is to the paint gun (so when you move the real paint gun than also the paint gun is moving in VR) than you can assign a reference tracker.
The paint gun in VR has a blue tracker mounted to it and is shown in this image.

![Paint gun in VR](Pictures/paintgun_assignment_in_headless.png)

The reference tracker has a model a blue tracker with the letter 'R' on top of it. It is shown here.

![Reference tracker in VR](Pictures/reftracker_assignment_in_headless.png)


The additional tracker has a model a blue tracker with the letter 'A' on top of it. It is shown here.

![Additional tracker in VR](Pictures/additional_tracker_assignment_in_headless.png)

Besides the trackers and paint gun also a box like shown below may be shown. This is the World Reference as defined in the configuration file.

![World reference in VR](Pictures/world_reference_defined.png)


> If you painted a lot and want to clear up the strokes on the screen you can do this with key R. This will NOT erase the paths from the database.


### The second display
The second display is the image from the VR headset and that could be usefull to identify the trackers. 
![Second display](Pictures/paintheadless_VRscherm.png)

> If you have one monitor connected than probably only the first display only is shown, and not the display with the VR headset image.



---

## The configuration file
The configuration file is a json-file name config.json. It must be placed in the same directory as PaintHeadless.exe.
It is read at the start of the application, so alterations when the program is running is not allowed.

This is the default configuration. In the headless configuration the PlayerStartLocation and PaintPlume is not used in the headless configuration. This is only relevant for the minimal configuration.
Values given for PlayerStartLocation and PaintPlume are ignored.



```json
{ "Database" :	{ "ClientConnection" : "mongodb://127.0.0.1:27017",
                  "DatabaseName" : "test",
                  "DBCollection" : "Prorob3"
                },
    "PlayerStartLocation" : { "x" : -2.3,
                              "y" : 0,
                              "z" : -13.3 
                            },
    "TrackingUnit" : "distance",
    "TrackingQuantity" : 0.1,   
    "WorldRefLocation" :  { "x" : -2.3,
                            "y" : 0,
                            "z" : -13.3,
                            "qx" : 0,
                            "qy" : 0,
                            "qz" : 0,
                            "qw" : 1
                           }, 
    "PaintPlume" : "green"
}
```

#### Database
The MongoDB database must already be possible. If you have it installed locally than most likely (default) 
it is installed in 127.0.0.1:27017.
The database must already be present, here it is called "test".
The DBCollection must not be necessarily available. If it already exists than new paint-strokes will be added to it.
If the DBCollection does not exist than it will be created by the program if allowed.

```json
{"Database" :	{ "ClientConnection" : "mongodb://127.0.0.1:27017",
                  "DatabaseName" : "test",
                  "DBCollection" : "Prorob3"
                }}
```

#### PlayerStartLocation
Not used in headless configuration.

#### Tracking
There is the ability to track with a fixed distance. Than trackingunit must be "distance". Than trackingquantity is the fixed distance in meter.
There is the ability to track with a fixed time. Than trackingunit must be "time". Than trackingquantity is the fixed time in seconds.


```json
  {  "TrackingUnit" : "distance",
    "TrackingQuantity" : 0.1,   }
```

#### WorldRefLocation
The worldreflocation is indicated in the playfield with a square box with sides of 10 cm. Its location and orientation (quaternion) must be given (with numbers as indicated below). 
Locations are in meter (x,y,z). Quaternion is (qx,qy,qz,qw).


```json
{    "WorldRefLocation" :  { "x" : -2.3,
                            "y" : 0,
                            "z" : -13.3,
                            "qx" : 0,
                            "qy" : 0,
                            "qz" : 0,
                            "qw" : 1
                           }}
```
#### PaintPlume
Not used in headless configuration.

---


## Setting up the Vive Tracker Binding
This needs to be done in SteamVR and is [common practice for defintion of games](https://steamcommunity.com/sharedfiles/filedetails/?id=2029205314).
A binding is description of the mapping of the controller buttons to functions in the game.

In this situation we do not use the regular controllers but a Vive Tracker. So in the manner described above choose "Vive Tracker in Hand".
For Paintheadless, the name of the application, a binding is already made. You have to activate it. See below figure.

![Bindings for Paintheadless](Pictures/SteamVR_bindings_paintheadless.png)

#### Under the hood
This is written for further understanding, you do not need to additionally 'do' anything.
Interesting to understand that there is only one button (the lever you pull) that needs to be known somehow. Actually electrically this is assigned to pin 4 of the pogo-pad and connectivety between the switch and the tracker is achieved by mouting the tracker with the plastic screw.
Now we have a trigger-input. In the binding the function "paintlever" is assigned to the trigger-input. 

See the function assignment in the image below.

![Function paintlever assigned to the trigger (and grip also)](Pictures/SteamVR_editing_paintheadless_configuration.png)


---

## Troubleshooting

#### Painting does not seem to work

First check:

1. SteamVR beta is running without problems. 
2. The application runs and the VR headset is attached and also working.
3. The paint gun is assigned correctly in the application.


Persistant Problem: Painting still does not work. You do not see a stroke on the screen (or in the headset) when the paint gun lever is pushed.

Reason 1: The SteamVR binding is not set correctly.

Solution 1: Go to the bindings and make sure there are set correctly. See chapter on bindings.


Reason 2: The mechanical trigger is not connecting with the tracker. 

Solution 2: Analysis needs to be done in the SteamVR application to make sure you have contact.
Open Settings > Controllers > Test Controllers.
Than you get a window like below. If you push the trigger than the trigger should light up on the screen. If it is not lit up than the problem is mechanical. 
If the trigger is lit up in the screen (as shown below) than the problem is in the SW, probably redo the bindings.


![Testing controller](Pictures/testing_controller_trigger.png). 


#### After the first stroke it takes very long, looks like the application blocks

Problem: After the first stroke it takes very long, looks like the application blocks.

Reason: The database you defined is not reachable. 

Solution: Set the configuration file correctly and check the database connection [with MongoDB Compass](https://www.mongodb.com/products/compass).
